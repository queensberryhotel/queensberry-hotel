Boutique, Luxury Hotel in Bath City Centre in the UK. Perfect for Weekend Getaways & fine dining at the Olive Tree Restaurant. Every Room is Unique. Impeccable service. Feel at home at one of the best place to stay in Bath.

Address: RUSSEL STREET, BATH, Somerset BA1 2QF, United Kingdom

Phone: +44 1225 447928

Website: https://www.thequeensberry.co.uk
